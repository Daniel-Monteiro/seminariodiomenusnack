package com.example.daniel.seminariodiogenes;


import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CadastroActivity extends AppCompatActivity implements View.OnClickListener{

    private viewHolder v = new viewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        this.v.editEmail  = (EditText) findViewById(R.id.editEmail);
        this.v.editNome = (EditText) findViewById(R.id.editNome);
        this.v.btnCadastro = (Button) findViewById(R.id.button);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == this.v.btnCadastro.getId()){
            Snackbar.make(v, R.string.snackmessage, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    public class viewHolder{
        Button btnCadastro;
        EditText editNome;
        EditText editEmail;
    }
}
